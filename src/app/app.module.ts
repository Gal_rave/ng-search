import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './interceptors/http-error-interceptor';
import { CookieService } from 'ngx-cookie-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

/*services*/
import { AuthorizationService } from './services/authorization.service';

/*components*/
import { AppComponent } from './app.component';
import { FooterComponent } from './core/footer/footer.component';
import { HeaderComponent } from './core/header/header.component';
import { MainIndexComponent } from './core/pages/main-index/main-index.component';
import { PageNotFoundComponent } from './core/pages/page-not-found/page-not-found.component';
import { LoginComponent } from './core/pages/login/login.component';
import { PagingPipe } from './pipes/paging.pipe';
import { DisplayImagesComponent } from './core/pages/display-images/display-images.component';
import { ImageCardComponent } from './core/pages/image-card/image-card.component';
import { ProfanityFilterPipe } from './pipes/profanity-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    MainIndexComponent,
    PageNotFoundComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    PagingPipe,
    DisplayImagesComponent,
    ImageCardComponent,
    ProfanityFilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MultiselectDropdownModule,
    CustomMaterialModule,
    BsDatepickerModule.forRoot(),
    NgbModule
  ],
  providers: [
    CookieService,
    AuthorizationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
