import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  login(username:string, password:string){
    localStorage.setItem('currentUser', JSON.stringify({username: username, password: password}));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  get currentUserValue(){
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
