import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap, retry } from 'rxjs/internal/operators';
import { Injectable } from '@angular/core';
import { Observable, of, Observer } from 'rxjs';
import { GiphyResults } from '../interfaces/giphy-results';
import { CreateImage } from '../interfaces/image';
import { createPaginationResult } from '../interfaces/pagination-result';

@Injectable({
  providedIn: 'root'
})
export class GiphySearchService {
  private BASE_URL: string = 'http://api.giphy.com/v1/';
  private API_KEY: string = 'CdRKiCMbTnt9CkZTZ0lGukSczk6iT4Z6';
  private LIMIT: number = 12;

  constructor(private http: HttpClient) {
  }

  SearchInGiphy(type: boolean, query: string, offset: number = 0, limit:number = this.LIMIT): Observable<GiphyResults> {
    return this.http.get<any>(this.setUrl(type, query, offset, limit))
      .pipe(
        tap((results: any) => {
          results.pagination = createPaginationResult(results.pagination);
          results.data = results.data.map(img => { return CreateImage(img); });
          delete results.meta;
        }),
        retry(0), catchError(this.handleError<GiphyResults>('SearchInGiphy', null))
      );
  }

  private setUrl(type: boolean, query: string, offset: number, limit:number): string {
    return `${this.BASE_URL}${type ? 'gifs' : 'stickers'}/search?api_key=${this.API_KEY}&limit=${limit}&q=${query}&offset=${offset}`;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error('handleError', error);
      return of(result as T);
    };
  }
}
