export interface Image {
    embed_url: string;
    rating: string;
    slug: string;
    type: string;
    title: string;
}
export function CreateImage(data: any): Image {
    const image: Image = { embed_url: data.embed_url, rating: data.rating, slug: data.slug, type: data.type, title: data.title};
    return image;
}