export interface RadioItem {
    value?: boolean;
    name: string;
}
