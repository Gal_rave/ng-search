export interface PaginationResult {
    total_count:number;
    offset:number;
    count:number;
}
export function createPaginationResult(data: any): PaginationResult{
    let pagination: PaginationResult = {total_count:data.total_count, offset:data.offset, count:data.count};
    return pagination;
}
