import { PaginationResult, createPaginationResult } from './pagination-result';
import { Image, CreateImage } from './image';

export interface GiphyResults {
    data: Array<Image>;
    pagination: PaginationResult;
}
export function createGiphyResults(data: any): GiphyResults {
    const res: GiphyResults = { pagination: createPaginationResult(data.pagination), data: data.data.map(img => { return CreateImage(img); }) };
    return res;
}
