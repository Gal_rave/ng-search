import { Component, OnInit, Input } from '@angular/core';
import { Image } from 'src/app/interfaces/image';

@Component({
  selector: 'app-display-images',
  templateUrl: './display-images.component.html',
  styleUrls: ['./display-images.component.css']
})
export class DisplayImagesComponent {
  @Input() images: Array<Image>;

}
