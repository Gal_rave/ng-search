import { Component } from '@angular/core';

/*services*/
import { SnackBarCaller } from 'src/app/custom-material/snack-bar-caller';
import { RadioItem } from 'src/app/interfaces/radio-item';
import { GiphySearchService } from 'src/app/services/giphy-search.service';
import { Pagination } from 'src/app/moduls/pagination/pagination';
import { GiphyResults } from 'src/app/interfaces/giphy-results';
import { Image } from 'src/app/interfaces/image';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-main-index',
  templateUrl: './main-index.component.html',
  styleUrls: ['./main-index.component.css']
})
export class MainIndexComponent {
  optionFormGroup: FormGroup;
  public pagination: Pagination;
  public Images: Array<Image> = new Array<Image>();

  searchTypeRadio: RadioItem[] = [
    { value: true, name: '`GIF`' },
    { value: false, name: '`Sticker`' }
  ];

  constructor(private giphySearch: GiphySearchService, private fb: FormBuilder, private barCaller: SnackBarCaller) {
    this.pagination = new Pagination(0, 12);

    this.optionFormGroup = this.fb.group({
      Query: ['', [Validators.required, Validators.minLength(2)]],
      SearchOption: ['', [Validators.required]]
    });
  }

  get Query() {
    return this.optionFormGroup.get('Query');
  }
  get SearchOption() {
    return this.optionFormGroup.get('SearchOption');
  }

  public Search() {
    this.giphySearch.SearchInGiphy(this.SearchOption.value, this.Query.value, this.pagination.offset, this.pagination.Limit).subscribe((data: GiphyResults) => {
      this.pagination = new Pagination(data.pagination.total_count, this.pagination.Limit, this.pagination.offset);
      this.Images = data.data.filter(img => img.embed_url !== null && img.title !== null && img.embed_url.length > 0 && img.title.length > 0);
      if (this.Images.length < 1)
        this.barCaller.PopMessage(false, 'Try to improve your search.', 'No Results Found!');
    });
  }

  public MovePage(move: any): void {
    if (isNaN(move)) {
      switch (move) {
        case '+':
          this.pagination.offset += 1;
          break;
        case '++':
          this.pagination.offset = (this.pagination.MaxPages - 1);
          break;
        case '-':
          this.pagination.offset -= 1;
          break;
        case '--':
          this.pagination.offset = 0;
          break;
      }
    } else {
      this.pagination.offset = move;
    }
    this.Search();
  }

}
