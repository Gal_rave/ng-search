import { Component, Input } from '@angular/core';
import { Image } from 'src/app/interfaces/image';
import { GiphySearchService } from 'src/app/services/giphy-search.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image-card',
  templateUrl: './image-card.component.html',
  styleUrls: ['./image-card.component.css']
})
export class ImageCardComponent {
  @Input() image: Image;  
  constructor(private siphySearch: GiphySearchService, public sanitizer: DomSanitizer ) { }

}
