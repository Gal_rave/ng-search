import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public userName: string;

  constructor(private authorization: AuthorizationService) {
    
  }

  ngOnInit() {
    this.userName = this.authorization.currentUserValue.username;
  }

}
