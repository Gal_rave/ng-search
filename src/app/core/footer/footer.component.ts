import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Pagination } from 'src/app/moduls/pagination/pagination';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  @Input() pager: Pagination;
  @Output("PageMove") PageMove = new EventEmitter<any>();

  public MovePage(move: any):void{
    this.PageMove.emit(move);
  }
}
