import { Injectable } from '@angular/core';
import {
  HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpResponse
} from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { catchError, finalize } from 'rxjs/internal/operators';

import { AuthorizationService } from '../services/authorization.service';
import { SnackBarCaller } from '../custom-material/snack-bar-caller';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthorizationService, private barCaller: SnackBarCaller) {
    
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if ([401, 403].indexOf(error.status) !== -1) {
            this.barCaller.PopMessage(false, 'Unauthorized.', String(error.status));
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.authenticationService.logout();
            location.reload(true);
          }
          
          if (error.error instanceof ErrorEvent) {
            this.barCaller.PopMessage(false, 'network error.', String(error.status));
            // A client-side or network error occurred. Handle it accordingly.
            //console.error('An error occurred:', error.error.message);
          } else {
            this.barCaller.PopMessage(false, 'backend error.', String(error.statusText));
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            //console.error(`Backend returned code ${error.status}, ` + `status was: ${error.statusText}`);
          }
          // return an observable with a user-facing error message
          return throwError('Something bad happened; please try again later.');
        }),
        finalize(() => {
          
        })
      );
  }
}
