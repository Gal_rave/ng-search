import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/*components*/
import { MainIndexComponent } from './core/pages/main-index/main-index.component';
// import { PageNotFoundComponent } from './core/pages/page-not-found/page-not-found.component';

/*services*/
import { AuthGuardService } from './interceptors/auth-guard.service';
import { LoginComponent } from './core/pages/login/login.component';

const routes: Routes = [
  {path:'Main', component: MainIndexComponent,canActivate: [AuthGuardService]},
  // {path:'PageNotFound', component: PageNotFoundComponent},
  {path: 'Login', component: LoginComponent},
  {path: '**', redirectTo: '/Main', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
