export class Pagination { 
  constructor(totalCount: number, pageSize?: number, offset?: number) {
    this.offset = offset?? 0;
    this.Limit = pageSize ?? 20;
    this.count = totalCount;
    this.MaxPages = Math.ceil(this.count / this.Limit);
    this.PagerList = Array(this.MaxPages).fill(0).map((x, i) => (i));
}
public Limit: number;
public MaxPages: number;
public offset: number;
public count: number;
public PagerList: number[];
}
