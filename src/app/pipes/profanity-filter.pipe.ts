import { Pipe, PipeTransform } from '@angular/core';
import * as Filter from 'node_modules/leo-profanity';

@Pipe({
  name: 'profanityFilter'
})
export class ProfanityFilterPipe implements PipeTransform {
  transform(value: string): string {
    Filter.loadDictionary();
    return Filter.clean(value);
  }

}
