import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paging'
})
export class PagingPipe implements PipeTransform {

  transform(list: number[], current: number, limit: number, maxPages: number): number[] {
    let startAt = 0;
    let endAt = maxPages;
    if (maxPages > limit) {
      startAt = current - limit > 0 ? current - limit : 0;
      endAt = current + (limit - 1) > maxPages ? maxPages : current + (limit - 1);
    }
    return list.slice(startAt, endAt);
  }

}
